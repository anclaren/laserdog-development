<?php

use Illuminate\Database\Seeder;

use App\Models\Game;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [
        	0 => ['name' => 'New beginning'],
        	1 => ['name' => 'Taylored Events in Riga'],
        	3 => ['name' => 'Amazing Gifts till end of the month'],
        	4 => ['name' => 'How to travel with paper map'],
            5 => ['name' => 'New beginning'],
            6 => ['name' => 'Taylored Events in Riga'],
            7 => ['name' => 'Amazing Gifts till end of the month'],
            8 => ['name' => 'How to travel with paper map'],
            9 => ['name' => 'New beginning'],
            10 => ['name' => 'Taylored Events in Riga'],
            11 => ['name' => 'Amazing Gifts till end of the month'],
            12 => ['name' => 'How to travel with paper map'],
        ];

        foreach ($games as $key => $value) {
        	Game::create([
        		'name' => $value['name'],
                'locale' => 'en',
        	]);
        }
    }
}
