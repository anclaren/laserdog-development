<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        	0 => ['name' => 'RE', 'email' => 'reinis@anclaren.com', 'password' => bcrypt('4122181')],
        	1 => ['name' => 'Ivo Malecs', 'email' => 'ivo@anclaren.com', 'password' => bcrypt('secret')],
        ];

        foreach ($users as $key => $user) {
        	User::create([
        		'name' => $user['name'],
        		'email' => $user['email'],
        		'password' => $user['password'],
        	]);
        }
    }
}
