<?php

use Illuminate\Database\Seeder;

use App\Models\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = [
        	0 => ['title' => 'New beginning'],
        	1 => ['title' => 'Taylored Events in Riga'],
        	3 => ['title' => 'Amazing Gifts till end of the month'],
        	4 => ['title' => 'How to travel with paper map'],
            5 => ['title' => 'New beginning'],
            6 => ['title' => 'Taylored Events in Riga'],
        ];

        $text = 'I neglect my talents Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts';

        foreach ($articles as $key => $value) {
        	Article::create([
        		'title' => $value['title'],
        		'content' => $text,
                'user_id' => 2,
                'locale' => 'en',
        	]);
        }
    }
}
