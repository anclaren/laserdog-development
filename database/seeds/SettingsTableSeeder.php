<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

use Anclaren\SettingTool\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
        	'name' => 'LASERDOG | Lāzera peintbols | Lāzertags',
        	'company_name' => '"Skats no Augšas" SIA',
        	'address' => 'Bauskas iela 16, Riga, Latvia',
        	'phone' => '+ (371) 200 000 89',
        	'email' => 'info@laserdog.lv',
        	'facebook' => 'https://www.facebook.com/laserdog.lv/',
        	'instagram' => 'https://www.instagram.com/lazerpeintbols/',
            'linkedin' => 'https://www.linkedin.com/company/laserdog',
            'google' => '#',
        	'youtube' => '#',
        ];

        foreach ($settings as $key => $value) {
        	Setting::create([
        		'key' => $key,
        		'value' => $value,
        		'updated_at' => Carbon::now(),
        		'created_at' => Carbon::now(),
        	]);
        }
    }
}
