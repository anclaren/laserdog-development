<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(class_exists('Anclaren\MenuBuilder\Http\Models\Menu')) {
            \Anclaren\MenuBuilder\Http\Models\Menu::create([
                'name' => 'Main Menu',
                'slug' => 'main-menu',
                'locale' => 'en'
            ]);

            \Anclaren\MenuBuilder\Http\Models\Menu::create([
                'name' => 'Main Menu LV',
                'slug' => 'main-menu',
                'locale' => 'lv',
                'locale_parent_id' => 1
            ]);

        	\Anclaren\MenuBuilder\Http\Models\Menu::create([
        		'name' => 'Footer Menu',
        		'slug' => 'footer-menu',
        		'locale' => 'en',
        	]);
        }

        if(class_exists('Anclaren\MenuBuilder\Http\Models\MenuItems')) {

            $menuItems = [
                1 => ['name' => 'Home'],
                2 => ['name' => 'Prices'],
                3 => ['name' => 'Gallery'],
                4 => ['name' => 'Reviews'],
                5 => ['name' => 'Game Formats'],
                6 => ['name' => 'Contacts'],
            ];

        	$menuItemsLV = [
	        	1 => ['name' => 'Sākums'],
	        	2 => ['name' => 'Cenas'],
	        	3 => ['name' => 'Galerija'],
                4 => ['name' => 'Atsauksmes'],
	        	5 => ['name' => 'Spēļu Formāti'],
	        	6 => ['name' => 'Kontakti'],
	        ];

            $footerMenuItems = [
                1 => ['name' => 'Home', 'page_id' => 1],
                2 => ['name' => 'Blog', 'page_id' => 7],
                3 => ['name' => 'Reviews', 'page_id' => 4],
                4 => ['name' => 'Contacts', 'page_id' => 6],
                5 => ['name' => 'Gallery', 'page_id' => 3],
                6 => ['name' => 'Prices', 'page_id' => 2],
                7 => ['name' => 'Game Formats', 'page_id' => 5],
            ];

            foreach($menuItems as $key => $item) {
                \Anclaren\MenuBuilder\Http\Models\MenuItems::create([
                    'menu_id' => 1,
                    'name' => $item['name'],
                    'page' => $key,
                    'order' => $key,
                ]);
            }

            foreach($menuItemsLV as $key => $item) {
                \Anclaren\MenuBuilder\Http\Models\MenuItems::create([
                    'menu_id' => 2,
                    'name' => $item['name'],
                    'page' => $key,
                    'order' => $key,
                ]);
            }

        	foreach($footerMenuItems as $key => $item) {
        		\Anclaren\MenuBuilder\Http\Models\MenuItems::create([
        			'menu_id' => 3,
        			'name' => $item['name'],
        			'page' => $item['page_id'],
        			'order' => $key,
        		]);
        	}

        }
    }
}
