<?php

use Illuminate\Database\Seeder;

use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
        	0 => ['name' => 'Home', 'template' => 'homepage', 'homepage' => true],
        	1 => ['name' => 'Prices', 'template' => 'prices', 'homepage' => false],
        	2 => ['name' => 'Gallery', 'template' => 'gallery', 'homepage' => false],
        	3 => ['name' => 'Reviews', 'template' => 'reviews', 'homepage' => false],
        	4 => ['name' => 'Games', 'template' => 'games', 'homepage' => false],
            5 => ['name' => 'Contacts', 'template' => 'contacts', 'homepage' => false],
        	6 => ['name' => 'Blog', 'template' => 'blog', 'homepage' => false],
        ];

        if(class_exists('Anclaren\NovaPageManager\Models\Page')) {

            foreach ($pages as $key => $page) {
                \Anclaren\NovaPageManager\Models\Page::create([
                    'name' => $page['name'],
                    'locale' => 'en',
                    'template' => $page['template'],
                    'seo_title' => $page['name'],
                    'seo_description' => $page['name'],
                ]);
            }
        }

    }
}
