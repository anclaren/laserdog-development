<?php
function str_word_count_utf8($str) {
	return count(preg_split('~[^\p{L}\p{N}\']+~u',$str));
}

function explodeByStringLength($string,$maxLineLength)
{
	$arrayOutput = [];
    if(!empty($string))
    {
        $arrayWords = explode(" ",$string);

        if(count($arrayWords) > 1)
        {
            
            $currentLength = 0;
            $index = 0;

            foreach($arrayWords as $index => $word)
            {
                $wordLength = strlen($word);

                if( ( $currentLength + $wordLength ) <= $maxLineLength )
                {
                    $currentLength += $wordLength;
                    $arrayOutput[] .= $word . ' ';
                }
                else
                {
                	$currentLength = $wordLength;

        			$arrayOutput[$index] = $word;

                    //break;
                }
            }

            return implode(' ',$arrayOutput);
        }
        else
        {
            return $string;
        }       
    }
    else return $string;
}