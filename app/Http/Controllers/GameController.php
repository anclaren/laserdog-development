<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;

class GameController extends Controller
{
    public function show(Request $request)
    {
    	$game = Game::whereSlug($request->slug)->first();

    	return view('games.single', compact('game'));
    }

    public function loadMoreImages(Request $request)
    {
    	$nextItems = [];
    	$gallery = Game::find($request->game_id)->getMedia('game_gallery')->slice(8);

    	foreach ($gallery as $key => $image) {
    		$nextItems[$key]['thumb'] = $image->getFullUrl('gallery_large_thumb');
    		$nextItems[$key]['full'] = $image->getFullUrl();
    	}

    	return $nextItems;
    }
}