<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function show(Request $request)
    {
    	$article = Article::whereSlug($request->slug)->first();

    	return view('articles.single', compact('article'));
    }
}