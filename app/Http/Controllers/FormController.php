<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

// Models
use App\Models\Booking;

// Mailable
use App\Mail\InquirySubmitted;
use App\Mail\ContactRequest;
use App\Mail\NotifyAboutInquiry;

class FormController extends Controller
{
    public function sendInquiry(Request $request)
    {
        $result = [];
    	$data = $request->except(['_token']);

        if($request->email) {

            // Storing data
            $booking = Booking::create($data);

            // Send notification email to customer
            Mail::to($request->email)->send(new InquirySubmitted($booking));

            // Notify support about incoming inquiry
            Mail::to('info@laserdog.lv')->send(new NotifyAboutInquiry($booking));

            // Prepare results
            $result['game_id'] = $data['game_id'];

            if($data['planned_game_time'] == '2') {
                $result['value'] = 15.00;
            } else {
                $result['value'] = 10.00;
            }

        	return redirect()->back()->with('status', __('form.message_success'))->with(['inquiryPopupSuccess' => true, 'result' => $result]);
        }

        return redirect()->back()->with('statusFail', __('form.message_failed'));
    }

    public function contactForm(Request $request)
    {
    	$data = $request->except(['_token']);

        if($request->email) {

            // Notify support about request
            Mail::to('info@laserdog.lv')->send(new ContactRequest($request));

    	   return redirect()->back()->with('status', __('form.message_success'));
        }

        return redirect()->back()->with('statusFail', __('form.message_failed'));
    }
}