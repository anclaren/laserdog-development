<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Booking;

class InquiryDeclined extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The booking instance.
     *
     * @var Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Booking Declined | Laser Dog';

        if($this->booking->locale == 'lv') {
            $subject = 'Pieteikums noraidīts | Laser Dog';
        }

        return $this->from('info@laserdog.lv', 'Laser Dog')->subject($subject)->view('emails.inquiry.declined');
    }
}
