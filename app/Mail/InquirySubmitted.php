<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

// Models
use App\Models\Booking;

class InquirySubmitted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The booking instance.
     *
     * @var Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Booking Submitted | Laser Dog';

        if($this->booking->locale == 'lv') {
            $subject = 'Pieteikums ir saņemts | Laser Dog';
        }

        return $this->from('info@laserdog.lv', 'Laser Dog')->subject($subject)->view('emails.inquiry.submitted');
    }
}
