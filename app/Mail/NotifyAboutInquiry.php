<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

// Models
use App\Models\Booking;

class NotifyAboutInquiry extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The booking instance.
     *
     * @var Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'New Booking Received | ' . $this->booking->name;

        if($this->booking->locale == 'lv') {
            $subject = 'Jauns pieteikums ir saņemts | ' . $this->booking->name;
        }

        return $this->from('info@laserdog.lv', 'Laser Dog')->subject($subject)->view('emails.inquiry-notification');
    }
}
