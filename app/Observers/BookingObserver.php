<?php

namespace App\Observers;

use Mail;

use App\Models\Booking;

// Mailable
use App\Mail\InquiryAccepted;
use App\Mail\InquiryDeclined;

class BookingObserver
{
    /**
     * Handle the booking "updating" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    public function saving(Booking $booking)
    {
        $original = $booking->getOriginal();

        // Check if status is selected and e-mail isn't send yet
        if(isset($booking->status)) {

            if($booking->status != null && $booking->email_sent == '0') {

                // Check if current status not the same as old one
                if($original['status'] != $booking->status) {

                    // Sending mail
                    if($booking->status == '1') {
                        // InquiryAccepted
                        Mail::to($booking->email)->send(new InquiryAccepted($booking));
                    } else if($booking->status == false) {
                        // InquiryDeclined
                        Mail::to($booking->email)->send(new InquiryDeclined($booking));
                    }

                    // Defining email as sent to prevent duplicates
                    $booking->email_sent = '1';
                }
            }
        }
        
    }

}