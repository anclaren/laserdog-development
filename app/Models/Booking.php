<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['name', 'phone', 'email', 'game_id', 'participants', 'status', 'booking_date', 'locale'];

    protected $casts = [
    	//'status' => 'boolean',
    	'email_sent' => 'boolean',
    	'booking_date' => 'datetime',
    ];

    protected $attributes = [
    	'game_id' => null
    ];

    public function game()
    {
    	return $this->belongsTo('App\Models\Game','game_id','id');
    }
}