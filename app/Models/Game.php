<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use App\Traits\ImageOptimization;

class Game extends Model implements HasMedia
{
	use HasMediaTrait, ImageOptimization;

	protected $casts = [
		'data' => 'array'
	];
	
    public static function boot()
	{
		parent::boot();

		static::creating(function($model) {
			$model->slug = str_slug($model->name);
			$latestSlug = static::whereRaw("slug = '$model->slug' or slug LIKE '$model->slug-%'")->latest('id')->value('slug');
			
			if ($latestSlug) {
				$pieces = explode('-', $latestSlug);
				$number = intval(end($pieces));
				$model->slug .= '-' . ($number + 1);
			}
		});
	}

	public function registerMediaConversions(Media $media = null)
	{
	    $this->addMediaConversion('thumb')
	        ->width(150)
	        ->height(150);

	    $this->addMediaConversion('gallery_large_thumb')
	    	->fit('crop', 350, 230);
	    $this->addMediaConversion('gallery_small_thumb')
	    	->fit('crop', 260, 160);
	    $this->addMediaConversion('preview_thumb')
	    	->fit('crop', 340, 400);
	}

	public function registerMediaCollections()
	{
	    $this->addMediaCollection('main')->singleFile();
	    $this->addMediaCollection('game_gallery');
	}
}