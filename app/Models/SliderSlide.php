<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Traits\ImageOptimization;

class SliderSlide extends Model implements HasMedia
{
	use HasMediaTrait, ImageOptimization;

	public function registerMediaConversions(Media $media = null)
	{
	    $this->addMediaConversion('thumb')
	        ->width(130)
	        ->height(130);

	    $this->addMediaConversion('preview')
	    	->fit('crop', 1069, 647);
	}

	public function registerMediaCollections()
	{
	    $this->addMediaCollection('main')->singleFile();
	}
}