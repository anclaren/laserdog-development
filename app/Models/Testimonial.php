<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Traits\ImageOptimization;

class Testimonial extends Model implements HasMedia
{
    use HasMediaTrait, ImageOptimization;

    public function registerMediaConversions(Media $media = null)
	{
	    $this->addMediaConversion('thumb')
	        ->width(150)
	        ->height(150);
	}

	public function registerMediaCollections()
	{
	    $this->addMediaCollection('photo')->singleFile();
	}
}