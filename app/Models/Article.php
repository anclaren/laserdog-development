<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use App\Traits\ImageOptimization;

class Article extends Model implements HasMedia
{
	use HasMediaTrait, ImageOptimization;
	
    public static function boot()
	{
		parent::boot();

		static::creating(function($model) {
			$model->slug = str_slug($model->title);
			$latestSlug = static::whereRaw("slug = '$model->slug' or slug LIKE '$model->slug-%'")->latest('id')->value('slug');
			
			if ($latestSlug) {
				$pieces = explode('-', $latestSlug);
				$number = intval(end($pieces));
				$model->slug .= '-' . ($number + 1);
			}
		});
	}

	public function registerMediaConversions(Media $media = null)
	{
	    $this->addMediaConversion('thumb')
	        ->width(150)
	        ->height(150);
	}

	public function registerMediaCollections()
	{
	    $this->addMediaCollection('main')->singleFile();
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
}
