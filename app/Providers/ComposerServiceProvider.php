<?php

namespace App\Providers;

// Laravel stuff
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['*'], function ($view)
        {
            if(class_exists('\Anclaren\SettingTool\SettingTool')) {
    
                $settings = (object)[];
                foreach (\Anclaren\SettingTool\Models\Setting::all() as $key => $value) {
                    $settings->{$value->key} = $value->value;
                }

                $view->with(compact('settings'));
            }


            $socials = (object)[];

            // Facebook
            if(isset($settings->facebook) && $settings->facebook) {
                $socials->facebook = $settings->facebook;
            }
            // Instagram
            if(isset($settings->instagram) && $settings->instagram) {
                $socials->instagram = $settings->instagram;
            }
            // Twitter
            if(isset($settings->twitter) && $settings->twitter) {
                $socials->twitter = $settings->twitter;
            }
            // LinkedIn
            if(isset($settings->linkedin) && $settings->linkedin) {
                $socials->linkedin = $settings->linkedin;
            }
            // Pinterest
            if(isset($settings->pinterest) && $settings->pinterest) {
                $socials->pinterest = $settings->pinterest;
            }
            // Google
            if(isset($settings->google) && $settings->google) {
                $socials->google = $settings->google;
            }
            // Youtube
            if(isset($settings->youtube) && $settings->youtube) {
                $socials->youtube = $settings->youtube;
            }

            $view->with(compact('socials'));
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
