<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Illuminate\Support\Facades\Schema;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $locales = [];

        if(class_exists('Mcamara\LaravelLocalization\Facades\LaravelLocalization')) {
            foreach (\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales() as $key => $value) {

                if(Schema::hasTable('languages')) {
                    if(class_exists('App\Models\Language') && !\App\Models\Language::whereLanguage($key)->first()) {
                        \App\Models\Language::create([
                            'language' => $key,
                            'name' => ucfirst($value['native'])
                        ]);
                    }
                }

                $locales[$key] = ucfirst($value['native']);
            }
        }

        \Anclaren\NovaPageManager\NovaPageManager::configure([
            'templates' => [
                \App\Nova\Templates\HomepageTemplate::class,
                \App\Nova\Templates\ContactsTemplate::class,
                \App\Nova\Templates\PricesTemplate::class,
                \App\Nova\Templates\GalleryTemplate::class,
                \App\Nova\Templates\TextTemplate::class,
                \App\Nova\Templates\ReviewTemplate::class,
                \App\Nova\Templates\GamesTemplate::class,
                \App\Nova\Templates\BlogTemplate::class,
            ],
            'locales' => $locales,
            'draft' => true
        ]);

        if(class_exists('Anclaren\NovaLocaleField\LocaleField')) {
            \Anclaren\NovaLocaleField\LocaleField::getLocales(function() use ($locales) {
                return $locales;
            });
        }
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                'reinis@anclaren.com',
                'ivo@anclaren.com',
                'mikjelis.ziemelis@gmail.com'
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
        	new \Anclaren\MenuBuilder\MenuBuilder,
            new \Anclaren\NovaPageManager\NovaPageManager,
            new \Anclaren\NovaTranslation\NovaTranslation,
            new \Anclaren\SettingTool\SettingTool,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
