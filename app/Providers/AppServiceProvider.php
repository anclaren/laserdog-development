<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

// Models
use App\Models\Booking;

// Observers
use App\Observers\BookingObserver;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $availableLocales = [
            'lv',
            'en',
            'ru'
        ];

        if(array_key_exists(session()->get('locale'), $availableLocales)) {
            app()->setLocale(session()->get('locale'));
        } else {
            app()->setLocale(config('app.fallback_locale'));
        }

        // Observers
        Booking::observe(BookingObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Http/Helpers.php';
    }
}
