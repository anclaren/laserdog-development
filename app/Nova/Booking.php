<?php

namespace App\Nova;

// Laravel & Nova
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

// Models
use App\Models\Game;

// Fields
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;

class Booking extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\Models\\Booking';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'CRM';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        $statuses = [0 => 'Declined', 1 => 'Accepted'];
        $availableLocales = [
            'en' => 'en',
            'lv' => 'lv',
            'ru' => 'ru'
        ];
        $statusResolvers = [
            true => 'Accepted',
            false => 'Declined',
        ];
        $participants = [];

        for($i = 1; $i <= 10; $i++){
            $participants[$i] = $i;
        }

        return [
            ID::make()->sortable(),
            Text::make('Name')->sortable(),
            Text::make('Phone')->sortable(),
            Text::make('Email')->sortable(),
            BelongsTo::make('Game','game')->display('name')->nullable()->sortable(),
            Select::make('Participants')->options($participants)->nullable()->sortable(),
            Select::make('Status')->options($statuses)->nullable()->sortable(),
            Select::make('Locale')->options($availableLocales)->nullable()->sortable(),
            DateTime::make('Booking Date','booking_date')->sortable(),
            DateTime::make('Booking created on', 'created_at')->onlyOnIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
