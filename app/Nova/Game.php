<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Heading;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Anclaren\NovaLocaleField\LocaleField;
use Anclaren\NovaPageManager\Nova\Filters\TemplateLocaleFilter;
use Anclaren\NovaPageManager\Nova\Filters\TemplateChildrenFilter;
use Fourstacks\NovaRepeatableFields\Repeater;

class Game extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\Models\\Game';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Content';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [
            ID::make()->sortable(),
            Text::make('Name')->sortable(),
            Text::make('Slug')->sortable(),
            Repeater::make('Info Blocks', 'data')
                ->addField([
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'width' => 'w-full',
                ])
                ->addField([
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea',
                    'width' => 'w-full',
                ])
                ->addButtonText('Add block'),

            Images::make('Main Image', 'main')
                ->conversionOnIndexView('thumb'),

            Images::make('Gallery', 'game_gallery')
                ->conversionOnIndexView('thumb'),

            LocaleField::make('Locale', 'locale', 'locale_parent_id'),
        ];

        $seoFields = [
            Heading::make('SEO Fields'),
            Text::make('SEO Title', 'seo_title')->hideFromIndex(),
            Text::make('SEO Keywords', 'seo_keywords')->hideFromIndex(),
            Text::make('SEO Description', 'seo_description')->hideFromIndex(),
        ];

        $fields[] = new Panel('SEO Data', $seoFields);

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new TemplateLocaleFilter,
            new TemplateChildrenFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
