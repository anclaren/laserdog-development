<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Laravel\Nova\Fields\Image;

class ImageBlock extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'imageblock';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Image';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Image::make('Image'),
        ];
    }

}
