<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Laravel\Nova\Fields\Text;
use Fourstacks\NovaRepeatableFields\Repeater;

class MultipleTextBlocksAdvanced extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'multipletextblocksadvanced';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Multiple Text Blocks Advanced';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Background Color', 'bg'),
            Repeater::make('Text Blocks', 'blocks')
                ->addField([
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'width' => 'w-full',
                ])
                ->addField([
                    'label' => 'Content',
                    'name' => 'content',
                    'type' => 'textarea',
                    'width' => 'w-full',
                ])
                ->addField([
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addField([
                    'label' => 'URL',
                    'name' => 'url',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addButtonText('Add block')
        ];
    }

}
