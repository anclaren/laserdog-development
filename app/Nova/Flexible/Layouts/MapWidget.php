<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Fourstacks\NovaRepeatableFields\Repeater;
use Laravel\Nova\Fields\Text;

class MapWidget extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'mapwidget';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Map Widget + Locations';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Title'),
            Repeater::make('Location Blocks', 'locations')
                ->addField([
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'width' => 'w-full',
                ])
                ->addField([
                    'label' => 'Content',
                    'name' => 'content',
                    'type' => 'textarea',
                    'width' => 'w-full',
                ])
                ->addButtonText('Add location')
        ];
    }

}
