<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Flexible;
use Anclaren\NovaFlexibleContent\Layouts\Layout;

class GalleryWidget extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'gallerywidget';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Gallery Widget';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Flexible::make('Galleries', 'items')
                ->addLayout(\App\Nova\Flexible\Layouts\GalleryWidgetItem::class)
                ->button('Add gallery')
        ];
    }

}
