<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Fourstacks\NovaRepeatableFields\Repeater;

class MultipleTextBlocks extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'multipletextblocks';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Multiple Text Blocks + Image';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Title'),
            Image::make('Image'),
            Repeater::make('Text Blocks', 'blocks')
                ->addField([
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'width' => 'w-full',
                ])
                ->addField([
                    'label' => 'Content',
                    'name' => 'content',
                    'type' => 'textarea',
                    'width' => 'w-full',
                ])
                ->addButtonText('Add block')
        ];
    }

}
