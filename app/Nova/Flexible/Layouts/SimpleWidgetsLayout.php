<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;

use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;

class SimpleWidgetsLayout extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'simplewidgetslayout';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Simple Widget Block Layout';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        $widgets = [
            'games' => 'Recent Game Formats',
            'articles' => 'Article Widget',
        ];

        return [
            Text::make('Title'),
            Textarea::make('Description'),
            Select::make('Widget')->options($widgets),
        ];
    }

}
