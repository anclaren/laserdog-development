<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Laravel\Nova\Fields\Text;
use Fourstacks\NovaRepeatableFields\Repeater;

class DynamicBlocks extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'dynamicblocks';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Dynamic Block';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Title'),
            Repeater::make('Info Blocks')
                ->addField([
                    'label' => 'Name',
                    'name' => 'name',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addField([
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addButtonText('Add block'),
        ];
    }

}
