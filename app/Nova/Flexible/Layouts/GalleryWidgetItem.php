<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Laravel\Nova\Fields\Text;
use Halimtuhu\ArrayImages\ArrayImages;

class GalleryWidgetItem extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'gallerywidgetitem';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Gallery';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Name'),
            ArrayImages::make('Images', 'images')->path('images')->disk('public'),
        ];
    }

}
