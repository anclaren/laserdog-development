<?php

namespace App\Nova\Flexible\Layouts;

use Anclaren\NovaFlexibleContent\Layouts\Layout;
use Anclaren\NovaFlexibleContent\Flexible;

class DynamicInfoBlocks extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'dynamicinfoblocks';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Dynamic Info Blocks';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Flexible::make('Dynamic Blocks')
                ->addLayout(\App\Nova\Flexible\Layouts\DynamicBlocks::class)
                ->button('Add column')
        ];
    }

}
