<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use Anclaren\NovaPageManager\Template;
use Anclaren\NovaFlexibleContent\Flexible;

use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Image;

class HomepageTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'homepage';
    public static $seo = true;

    public function fields(Request $request): array
    {
        return [
        	Heading::make('CONTENT')->hideFromIndex()->hideWhenCreating()->hideFromDetail(),
        	Flexible::make('Content')
                ->addLayout(\App\Nova\Flexible\Layouts\MultipleTextBlocks::class)
                ->addLayout(\App\Nova\Flexible\Layouts\MultipleTextBlocksAdvanced::class)
                ->addLayout(\App\Nova\Flexible\Layouts\SimpleWidgetsLayout::class)
                ->addLayout(\App\Nova\Flexible\Layouts\MapWidget::class)
        ];
    }
}
