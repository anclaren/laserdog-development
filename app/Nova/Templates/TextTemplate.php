<?php
namespace App\Nova\Templates;

use Illuminate\Http\Request;
use Anclaren\NovaPageManager\Template;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Markdown;

class TextTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'text';
    public static $seo = true;

    public function fields(Request $request): array
    {
        return [
        	Heading::make('CONTENT')->hideFromIndex()->hideWhenCreating()->hideFromDetail(),
        	Markdown::make('Text')
        ];
    }
}