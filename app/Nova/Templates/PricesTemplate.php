<?php
namespace App\Nova\Templates;

use Illuminate\Http\Request;
use Anclaren\NovaPageManager\Template;
use Anclaren\NovaFlexibleContent\Flexible;
use Laravel\Nova\Fields\Heading;

class PricesTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'prices';
    public static $seo = true;

    public function fields(Request $request): array
    {
        return [
        	Heading::make('CONTENT')->hideFromIndex()->hideWhenCreating()->hideFromDetail(),
        	Flexible::make('Content')
                ->addLayout(\App\Nova\Flexible\Layouts\DynamicInfoBlocks::class)
                ->addLayout(\App\Nova\Flexible\Layouts\ImageBlock::class)
        ];
    }
}