<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use Anclaren\NovaPageManager\Template;
use Anclaren\NovaFlexibleContent\Flexible;

use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\KeyValue;
use Fourstacks\NovaRepeatableFields\Repeater;

class ContactsTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'contacts';
    public static $seo = true;

    public function fields(Request $request): array
    {
        return [
        	Heading::make('CONTENT')->hideFromIndex()->hideWhenCreating()->hideFromDetail(),
            KeyValue::make('Requisites')->rules('json'),
            Repeater::make('Info Blocks', 'blocks')
                ->addField([
                    'label' => 'Icon',
                    'name' => 'icon',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addField([
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'text',
                    'width' => 'w-1/2',
                ])
                ->addButtonText('Add block'),
        	Flexible::make('Content')
                ->addLayout(\App\Nova\Flexible\Layouts\MultipleTextBlocks::class)
                ->addLayout(\App\Nova\Flexible\Layouts\MultipleTextBlocksAdvanced::class)
                ->addLayout(\App\Nova\Flexible\Layouts\SimpleWidgetsLayout::class)
                ->addLayout(\App\Nova\Flexible\Layouts\MapWidget::class)
        ];
    }
}
