<?php
namespace App\Traits;

use Storage;

trait ImageOptimization {

	public static function bootImageOptimization()
    {
        static::saving(function ($model) {
            
            // Iterating through media placements ( collections )
        	foreach ($model->media->pluck('collection_name')->unique() as $key => $placement) {
        		
        		// Iterating through placement media files
        		foreach ($model->getMedia($placement) as $key => $media) {
        			
        			// Check if image needs optimization
        			if($media->mime_type !== 'image/webp') {

        				// File info
        				$infoPath = pathinfo($media->getUrl());
                        $_filename = $infoPath['filename'];

                        if(!isset($infoPath['extension'])) { continue; }

                        $_extension = $infoPath['extension'];

                        $replaceable = ['https://laserdog.anc-dev.info', '/storage/', 'https://laserdog.lv'];

                        $file = Storage::disk('public')->path(str_replace($replaceable, '', $media->getUrl()));
                        $_newPath = storage_path('app/public/' . $media->id . '/' . $_filename);

                        // Check if current file exists & if image not formated already
                        if(file_exists($file) && !file_exists($_newPath)) {
                            \Intervention\Image\Facades\Image::make($file)->encode('webp', 80)->save($_newPath . '.webp');
                        	\Intervention\Image\Facades\Image::make($file)->encode('jpeg', 80)->save($_newPath . '.jpeg');
                        }

                        // Iterate through conversions
                        foreach ($media->custom_properties['generated_conversions'] as $conversion => $boolean) {
                        	
                        	// Conversion file info
                        	$_conversionFileName = $_filename . '-' . $conversion;
                        	$_conversionFile = Storage::disk('public')->path(str_replace($replaceable, '', $media->getUrl($conversion)));
                        	$_newConversionFile = storage_path('app/public/' . $media->id . '/conversions/' . $_conversionFileName);

                        	// Generate optimized conversion file if doesn't exist
                        	if(file_exists($_conversionFile) && !file_exists($_newConversionFile)) {
                                \Intervention\Image\Facades\Image::make($_conversionFile)->encode('webp', 80)->save($_newConversionFile . '.webp');
                        		\Intervention\Image\Facades\Image::make($_conversionFile)->encode('jpeg', 80)->save($_newConversionFile . '.jpeg');
                        	}

                        }

        			}

        		}

        	}

        });

    }
}