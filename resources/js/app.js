$(document).ready(function() {

	// console.log('ready');

	// Smooth scroll to elements
    $("a.scrollTo").bind('click', function(e) {
        e.preventDefault();

        var hash = this.hash;

        if (!$(hash).length) {
        	document.cookie = 'anchor=' + hash + '; max-age=5';
            document.location.href="/";
		}

        var offset = $(hash).offset().top - 55;

        $('html, body').animate({
            scrollTop: offset
        }, 300);
    });

});

//hide nav open btn when the nav is open, adding/removing open classes to nav and content
var navOpenBtn = document.querySelector('.nav-open-btn');
var navCloseBtn = document.querySelector('.nav__close');
var nav = document.querySelector('.nav');
var pageContent = document.querySelector('.page__content');
var navList = document.querySelector('.nav__list');
var page = document.querySelector('.page');
var signUpBtn = document.querySelector('.burger-signup-btn');

//open nav
navOpenBtn.addEventListener('click', function() {
    navOpenBtn.classList.add('js-hidden');
    nav.classList.add('js-opened');
    pageContent.classList.add('js-opened');
});

//close nav
navCloseBtn.addEventListener('click', function() {
    navOpenBtn.classList.remove('js-hidden');
    nav.classList.remove('js-opened');
    pageContent.classList.remove('js-opened');
});

//closing navigation if click outside it
page.addEventListener('click', function(e) {
  
    var evTarget = e.target;
  
    if((evTarget !== nav) && (nav.classList.contains('js-opened')) && (evTarget !== navOpenBtn) && (evTarget.parentNode !== navOpenBtn)) {
        console.log(navOpenBtn.firstChild);
        navOpenBtn.classList.remove('js-hidden');
        nav.classList.remove('js-opened');
        pageContent.classList.remove('js-opened');
    }
  
});
signUpBtn.addEventListener('click', function(e) {
  
    var evTarget = e.target;
  
    if((evTarget !== nav) && (nav.classList.contains('js-opened')) && (evTarget !== navOpenBtn) && (evTarget.parentNode !== navOpenBtn)) {
        console.log(navOpenBtn.firstChild);
        navOpenBtn.classList.remove('js-hidden');
        nav.classList.remove('js-opened');
        pageContent.classList.remove('js-opened');
    }
  
});

//adding default demo classes
nav.classList.add('nav--offcanvas-3');
pageContent.classList.add('page__content--offcanvas-3');