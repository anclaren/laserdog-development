<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ (Route::currentRouteName() != 'articles.single' && isset($settings->name) && $settings->name) ? $settings->name : config('app.name') }} @yield('meta_title')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="Content-Language" content="{{ str_replace('_', '-', app()->getLocale()) }}"/>
    <meta name="keywords" content="@yield('meta_keywords')"/>
    <meta name="description" content="@yield('meta_description')"/>
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

	@yield('styles')

	<!-- Icons for this template -->
	<link href="{{ asset('fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<!-- Core CSS -->
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
	<!-- CSS Libraries -->
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

	@stack('css')

	<script>
		window.environment = '{{ app()->environment() }}';
		window.locale = '{{ app()->getLocale() }}';
		window.baseUrl = '{{ url('/') }}';
		window.allowTrackingCookies = {{ ((Request::cookie('allowTrackingCookies') == null) ? 'false' : 'true') }};
	</script>

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '429198731035953');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=429198731035953&ev=PageView&noscript=1" alt="" 
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body class="page">

	<!-- Header -->
	@include('layouts.partials.header')

	<!-- Content -->
	<div class="page__content">
		@yield('content')
	</div>

	<!-- Footer -->
	@include('layouts.partials.footer')

	<!-- Notifications -->
	@include('layouts.partials.notifications')


	@yield('scripts')

	<!-- Core JavaScript -->
	<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
	<script src="{{ asset('js/vendor/popper.min.js') }}"></script>
	<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

	<!-- build:js -->
	<script src="{{ mix('js/app.js') }}"></script>
	<!-- endbuild -->

	<!-- Optional JavaScript -->
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyBaZbC7zusKSgV3uNn8bGH0S1LclBApvmU"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

	<!-- Successor of Bootstrap DateTimePicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js" integrity="sha256-AdQN98MVZs44Eq2yTwtoKufhnU+uZ7v2kXnD5vqzZVo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>


	@stack('js')

	<script>
		AOS.init();
	</script>

	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				xfbml            : true,
				version          : 'v4.0'
			});
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	
	<div class="fb-customerchat"
		attribution=setup_tool
		page_id="283045735366098"
		theme_color="#ca1b0b"
		logged_in_greeting="Sveiki! :) Droši uzdodiet sev interesējošo jautājumu!"
		logged_out_greeting="Sveiki! :) Droši uzdodiet sev interesējošo jautājumu!">
	</div>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83693431-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-83693431-1');
	</script>
</body>
</html>