<div class="container-fluid">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-6">
					<div class="laserdog-navbar-logo">
						<a href="{{ url('/') }}">
							<img src="{{ asset('img/laserdog-logo.png') }}" class="img-fluid" alt="LASERDOG">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-6"></div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-6">
			<div class="laserdog-top-bar d-none d-lg-block" style="z-index: 2;">
				<ul class="top-menu">
					<li><a href="#"><i class="fa fa-phone"></i> {{ (($settings->phone) ? $settings->phone : '') }}</a></li>
					<li class="button"><a href="#" data-toggle="modal" data-target=".signup-modal">{{ __('button.apply') }} <i class="fa fa-crosshairs"></i></a></li>
					<li>
						<div class="dropdown language-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" style="text-transform: uppercase;">{{ app()->getLocale() }}</a>
							<ul class="dropdown-menu" role="menu">
								@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
							        <li>
							            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
							                {{ $localeCode }}
							            </a>
							        </li>
							    @endforeach
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<nav class="nav nav--offcanvas-3">
				<div class="nav__close"></div>

				{!! menu_builder('main-menu', 'nav__list', 'nav__item', 'nav__link') !!}
			
				<ul class="nav__list">
					<li class="nav__item languages">
						<ul>
							@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
						        <li>
						            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" style="text-transform: uppercase; font-weight: bold;">
						                {{ $localeCode }}
						            </a>
						        </li>
						    @endforeach
						</ul>
					</li>
					<li class="nav__item button">
						<a class="nav__link burger-signup-btn" href="#" data-toggle="modal" data-target=".signup-modal">
							{{ __('button.apply') }} <i class="fa fa-crosshairs"></i>
						</a>
					</li>
				</ul>
			</nav>

			<!-- Navigation -->
			<nav class="navbar navbar-expand-lg laserdog-navbar navbar-dark bg-dark">
				<div class="container">

					<div class="navbar-toggler">
						<div class="nav-open-btn">
							<div class="nav-open-btn__bar"></div>
							<div class="nav-open-btn__bar"></div>
							<div class="nav-open-btn__bar"></div>
						</div>
					</div>

					<div class="collapse navbar-collapse" id="navbarResponsive">
						{!! menu_builder('main-menu', 'navbar-nav mr-auto', 'nav-item') !!}

						<div class="responsive-lang-switcher d-lg-none">
							<div class="dropdown language-dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" style="text-transform: uppercase;">{{ app()->getLocale() }}</a>
								<ul class="dropdown-menu" role="menu">
									@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
								        <li>
								            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
								                {{ $localeCode }}
								            </a>
								        </li>
								    @endforeach
								</ul>
							</div>
						</div>
						
					</div>
				</div>
			</nav>

		</div>
	</div>
</div>
