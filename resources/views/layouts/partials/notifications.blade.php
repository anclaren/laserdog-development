<div class="modal fade laserdog-modal signup-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">{{ __('application_modal.title') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="contact-form popup-form px-4" method="POST" action="{{ route('form.inquiry') }}">
					
					@csrf
					<input type="hidden" name="locale" value="{{ app()->getLocale() }}">
					
					<div class="form-group">
						<label class="col-form-label">@lang('form.name')</label>
						<input type="text" class="form-control" name="name">
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.phone')</label>
						<input type="text" class="form-control" name="phone">
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.email')</label>
						<input type="email" class="form-control" name="email">
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.event_format')</label>
						<select class="form-control" name="game_id">
							@foreach(\App\Models\Game::whereLocale(app()->getLocale())->get() as $key => $format)
								<option value="{{ $format->id }}">{{ $format->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.participants')</label>
						<select class="form-control" name="participants">
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value=">12">> 12</option>
						</select>
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.planned_game_time')</label>
						<select class="form-control" name="planned_game_time">
							<option value="1">1h</option>
							<option value="2">2h</option>
							<option value="2">@lang('form.option_other')</option>
						</select>
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.datetime')</label>
						<div class="input-group date laser-date-picker" id="bookingDateTimePicker" data-target-input="nearest">
		                    <input type="text" class="form-control datetimepicker-input" name="booking_date" data-target="#bookingDateTimePicker"/>
		                    <div class="input-group-append" data-target="#bookingDateTimePicker" data-toggle="datetimepicker">
		                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                    </div>
		                </div>
					</div>

					<button class="btn btn-block" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="@lang('form.apply_popover')">@lang('form.apply_to_game') <i class="fa fa-arrow-right"></i></button>
				</form>

				<div class="px-4 text-center">
					<p class="pt-4 pb-2">
						@lang('application_form.text1') <br>
						@lang('application_form.text2')
					</p>

					<h1 class="pb-3"><i class="fa fa-phone"></i> +371 20 00 089</h1>

					<h2>@lang('application_form.text4')</h2>
				</div>
			</div>
		</div>
	</div>
</div>

@if(Session::has('inquiryPopupSuccess'))
	<div class="modal fade laserdog-modal-success signup-modal" tabindex="-1" role="dialog" aria-labelledby="LaserDogModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ __('application_modal.title') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="px-4 text-center">
						<p class="pt-4 pb-2">
							<b>@lang('popup_success.text1')</b> <br>
							@lang('popup_success.text2')
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif

@push('js')

@if(Session::has('inquiryPopupSuccess'))
@php
	$pixelData = Session::get('result');
@endphp
<script>
fbq('trackCustom', 'Reservation',
	{
		value: {{ $pixelData['value'] }},
		currency: 'EUR',
		contents: [
			{
				id: '{{ $pixelData['game_id'] }}',
				quantity: 1
			}
		],
		content_type: 'reservation'
	}
);
</script>
@endif

<script>

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();

		$('[data-toggle="popover"]').popover();
	});

	$(function () {
        $('#bookingDateTimePicker').datetimepicker({
        	locale: '{{ app()->getLocale() }}',
        	format: 'YYYY-MM-DD HH:mm:00',
        	stepping: 30,
        	sideBySide: true,
        	allowInputToggle: true,
        	keepOpen: false,
        });
    });

	$(function(){

		var popupForm = $('body .laserdog-modal form.popup-form'),
			inputPhone = popupForm.find('input[name="phone"]'),
			inputEmail = popupForm.find('input[name="email"]'),
			errors = [];

		popupForm.on('submit', function(e){
			e.preventDefault();

			if(inputPhone.val().length < 8) {
				if(errors.includes('phone') == false) {
	            	errors.push('phone');
	            }
			} else {
				inputPhone.css('border-color', '#ced4da');
				errors.splice( errors.indexOf('phone'), 1 );
			}

			if(inputEmail.val().length < 8) {
				if(errors.includes('email') == false) {
	            	errors.push('email');
	            }
			} else {
				inputEmail.css('border-color', '#ced4da');
				errors.splice( errors.indexOf('email'), 1 );
			}

		    if(errors.length == 0) {
		    	popupForm.find('button').attr('disabled', true);
		    	$('body .laserdog-modal form.popup-form')[0].submit();
		    } else {

		    	$.each(errors, function(index, value) {
		    		popupForm.find('input[name="' + value + '"]').css('border-color', '#dd1f00');
		    	});

		    }


		});

	});

	@if(Session::has('inquiryPopupSuccess'))
		$('.laserdog-modal-success').modal('toggle');
	@endif
</script>
@endpush
