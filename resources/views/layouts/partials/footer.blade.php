<footer class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8 col-lg-8">
				{!! menu_builder('footer-menu', 'footer-menu') !!}
			</div>
			<div class="col-12 col-sm-12 col-md-4 col-lg-4 text-right">
				<ul class="footer-socials">
					@if(isset($socials->facebook))
						<li><a href="{{ $socials->facebook }}" target="_blank" rel="noreferrer" aria-label="Facebook"><i class="fa fa-facebook-f"></i></a></li>
					@endif

					@if(isset($socials->instagram))
						<li><a href="{{ $socials->instagram }}" target="_blank" rel="noreferrer" aria-label="Instagram"><i class="fa fa-instagram"></i></a></li>
					@endif

					@if(isset($socials->twitter))
						<li><a href="{{ $socials->twitter }}" target="_blank" rel="noreferrer" aria-label="Twitter"><i class="fa fa-twitter"></i></a></li>
					@endif

					@if(isset($socials->youtube))
						<li><a href="{{ $socials->youtube }}" target="_blank" rel="noreferrer" aria-label="Youtube"><i class="fa fa-youtube"></i></a></li>
					@endif

					@if(isset($socials->google))
						<li><a href="{{ $socials->google }}" target="_blank" rel="noreferrer" aria-label="GooglePlus"><i class="fa fa-google-plus"></i></a></li>
					@endif

					@if(isset($socials->pinterest))
						<li><a href="{{ $socials->pinterest }}" target="_blank" rel="noreferrer" aria-label="Pinterest"><i class="fa fa-pinterest"></i></a></li>
					@endif

					@if(isset($socials->linkedin))
						<li><a href="{{ $socials->linkedin }}" target="_blank" rel="noreferrer" aria-label="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
					@endif
				</ul>
				<div class="copyright">
					{{ date('Y') }} &copy; LASERDOG
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="copyright-anclaren">
					Made with <span class="heart"><i class="fa fa-heart"></i></span> by <a href="//www.anclaren.com" target="_blank" rel="nofollow noreferrer">ANCLAREN</a>
				</div>
			</div>
		</div>
	</div>
</footer>