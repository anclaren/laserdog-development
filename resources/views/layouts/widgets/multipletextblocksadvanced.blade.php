<section class="info-section py-5">
	<div class="container">
		<div class="row">
			@foreach($widgetData->blocks as $key => $block)
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 px-4 py-3">
					<h3>{{ (isset($block->title) ? $block->title : '') }}</h3>
					<p>{{ (isset($block->content) ? $block->content : '') }}</p>

					@if(isset($block->cta) && isset($block->url))
						<a href="{{ url($block->url) }}">{{ $block->cta }} <i class="fa fa-arrow-right"></i></a>
					@endif
				</div>
			@endforeach

		</div>
	</div>
</section>