@if(isset($widgetData->widget))

	@if($widgetData->widget == 'articles')

		@include('layouts.widgets.partials.articles')

	@elseif($widgetData->widget == 'games')

		@include('layouts.widgets.partials.games')

	@endif

@endif