<section class="category-carousel-container" data-aos="fade-up" data-aos-duration="500">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel category-carousel">

					@foreach(\App\Models\Game::whereLocale(app()->getLocale())->get() as $key => $game)

						@php
							unset($parts);
							unset($firstpart);
							unset($secondpart);
							unset($thirdpart);
							if(str_word_count_utf8($game->name) >= 2) {
								$parts = explode("\n", wordwrap($game->name, 10, "\n"));
								
								if(isset($parts[0])) { unset($firstpart); $firstpart = $parts[0]; }
								if(isset($parts[1])) { unset($secondpart); $secondpart = $parts[1]; } else { unset($secondpart); }
								if(isset($parts[2])) { unset($thirdpart); $thirdpart = $parts[2]; } else { unset($thirdpart); }
							}
						@endphp

						<div class="card category-item">
							<a href="{{ route('games.single', $game->slug) }}">

								<picture class="card-img-top">
									@if($game->getFirstMedia('main'))
		                                <source srcset="{{ str_replace('.jpg', '.webp', $game->getFirstMedia('main')->getFullUrl('preview_thumb')) }}" type="image/webp">
										<source srcset="{{ str_replace('.jpg', '.jpeg', $game->getFirstMedia('main')->getFullUrl('preview_thumb')) }}" type="image/jpeg"> 
										<img src="{{ $game->getFirstMedia('main')->getFullUrl('preview_thumb') }}" alt="{{ $game->name }}">
		                            @else
										<img src="{{ asset('img/category_item.jpg') }}" class="card-img-top" alt="{{ $game->name }}">
									@endif
								</picture>

								<div class="card-body">
									<div class="text-content">
										<h2>
											{{ (isset($firstpart) ? $firstpart : $game->name ) }}<br> @if(isset($secondpart)) <span class="text-color">{{ $secondpart }}</span> @endif @if(isset($thirdpart)) <br> {{ $thirdpart }} @endif
										</h2>
									</div>
								</div>
							</a>
						</div>
					@endforeach

				</div>
			</div>
		</div>
	</div>
</section>