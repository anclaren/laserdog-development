<section class="pt-5" data-aos="fade-up" data-aos-duration="500">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center py-4">
				<h1 class="section-title">{{ $widgetData->title }}</h1>
			</div>
		</div>

		<div class="row">

			@foreach(\App\Models\Article::limit(3)->get() as $key => $article)
				<div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
					<div class="card article-item">
						<div class="card-header">

							@if($article->getFirstMedia('main'))
                                <img src="{{ $article->getFirstMedia('main')->getFullUrl() }}" class="card-img-top">
                            @else
								<img src="{{ asset('img/article_demo_img.png') }}" class="card-img-top">
							@endif

							<div class="card-info">
								<i class="fa fa-calendar" aria-hidden="true" style="font-size: 18px; vertical-align: middle; margin-top: -5px; margin-right: 5px;"></i><span class="date">{{ $article->created_at->format('d.m.Y') }}</span>
							</div>
						</div>
						<div class="card-body">
							<h4 class="title px-2 text-center">
								<a href="{{ route('articles.single', $article->slug) }}">{{ $article->title }}</a>
							</h4>
						</div>
					</div>
				</div>
			@endforeach

		</div>

		<div class="row">
			<div class="col-12 text-center pt-4">
				<a href="{{ url('blog') }}" class="btn laserdog-btn">{{ __('btn.all_news') }} <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>