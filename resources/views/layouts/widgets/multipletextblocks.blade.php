<section class="pb-5" id="whatIsLasertag">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-title">{{ $widgetData->title }}</h2>
			</div>
		</div>

		<div class="row py-5">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">

				@foreach($widgetData->blocks as $key => $block)
					<div class="text px-3" data-aos="fade-right">
						<h3 class="text-red">{{ (isset($block->title) ? $block->title : '') }}</h3>
						<p>{{ (isset($block->content) ? $block->content : '') }}</p>
					</div>
				@endforeach

			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
				<div class="d-none d-xl-block" style="
					background-image: url('{{ (isset($widgetData->image) ? Storage::disk('public')->url($widgetData->image) : asset('img/about_figure.png')) }}');
					width: 100%;
					height: 100%;
					background-repeat: no-repeat;
					background-size: cover;
					position: absolute;
					bottom: -100px;
				"></div>
			</div>
		</div>
	</div>
</section>