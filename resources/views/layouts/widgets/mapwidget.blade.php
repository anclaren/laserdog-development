<section class="map-container">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center py-5">
				<h2 class="section-title pb-5">{{ $widgetData->title }}</h2>
			</div>
		</div>

		<div class="col-md-8 ml-md-auto mr-md-auto">
			<div class="row location-container">

				@foreach($widgetData->locations as $key => $item)
					<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
						<div class="location-block {{ ($key == 1) ? 'second-block' : '' }}" data-aos="flip-left" data-aos-duration="500">
							<h3>{{ $item->title }}</h3>
							<h4>{!! nl2br($item->content) !!}</h4>
						</div>
					</div>
				@endforeach

			</div>
		</div>
	
	</div>

	<div id="map">{{ __('MAP') }}</div>
</section>