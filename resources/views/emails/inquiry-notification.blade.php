Name, Surname: {{ $booking->name }}<br>
<br>
Phone: {{ $booking->phone }}<br>
<br>
Email: {{ $booking->email }}<br>
<br>
Game format: {{ $booking->game->name }}<br>
<br>
Participants: {{ $booking->participants }}<br>
<br>
Booking Date: {{ $booking->booking_date }}<br>
<br>
Locale: {{ $booking->locale }}