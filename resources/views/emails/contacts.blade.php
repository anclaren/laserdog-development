<h1 style="font-size: 16px;">Contact Request</h1>
<h2 style="font-size: 15px; font-weight: 200;"><b>Name:</b> {{ $request->input('name') }}</h2>
<h2 style="font-size: 15px; font-weight: 200;"><b>E-mail:</b> {{ $request->input('email') }}</h2>
<h2 style="font-size: 15px; font-weight: 200;"><b>Message:</b> {{ $request->input('message') }}</h2>