<div>
	@if($booking->locale == 'en')

		Hi, {{ $booking->name }}! <br>
		<br>
		Your application has been received and we will contact you soon to refine your booking data. <br>
		<br>
		Chosen game format: {{ $booking->game->name }} <br>
		<br>
		Number of Participants: {{ $booking->participants }} <br>
		<br>
		Planned Date and Time: {{ (isset($booking->booking_date) ? $booking->booking_date : '-') }} <br>
		<br>
		<br>
		<br>
		Best Wishes, <br>
		Team Laser Dog

	@elseif($booking->locale == 'lv')

		Sveiki, {{ $booking->name }}! <br>
		<br>
		Jūsu pieteikums ir saņemts. Drīzumā sazināsimies ar Jums, lai precizētu pieteikuma datus. <br>
		<br>
		Izvēlētais spēles formāts: {{ $booking->game->name }} <br>
		<br>
		Dalībnieku skaits: {{ $booking->participants }} <br>
		<br>
		Plānotais datums un laiks: {{ (isset($booking->booking_date) ? $booking->booking_date : '-') }} <br>
		<br>
		<br>
		Uz drīzu saziņu, <br>
		Laser Dog komanda

	@elseif($booking->locale == 'ru')

		Hi, {{ $booking->name }}! <br>
		<br>
		Your application has been received and we will contact you soon to refine your booking data. <br>
		<br>
		Chosen game format: {{ $booking->game->name }} <br>
		<br>
		Number of Participants: {{ $booking->participants }} <br>
		<br>
		Planned Date and Time: {{ (isset($booking->booking_date) ? $booking->booking_date : '-') }} <br>
		<br>
		<br>
		<br>
		See you soon, <br>
		Team Laser Dog

	@endif


</div>