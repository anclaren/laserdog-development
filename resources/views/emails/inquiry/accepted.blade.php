<div>
	@if($booking->locale == 'en')

		Hi, {{ $booking->name }}! <br>
		<br>
		We are glad to inform you that your booking has been confirmed! <br>
		<br>
		Chosen game format: {{ $booking->game->name }} <br>
		<br>
		Number of Participants: {{ $booking->participants }} <br>
		<br>
		Planned Date and Time: {{ $booking->booking_date }} <br>
		<br>
		<br>
		<br>
		See you soon, <br>
		Team Laser Dog

	@elseif($booking->locale == 'lv')

		Sveiki, {{ $booking->name }}! <br>
		<br>
		Vēlamies informēt, ka jūsu rezervācija ir veiksmīgi apstiprināta! <br>
		<br>
		Izvēlētais spēles formāts: {{ $booking->game->name }} <br>
		<br>
		Dalībnieku skaits: {{ $booking->participants }} <br>
		<br>
		Plānotais datums un laiks: {{ $booking->booking_date }} <br>
		<br>
		<br>
		Uz drīzu tikšanos, <br>
		Laser Dog komanda

	@elseif($booking->locale == 'ru')

		Hi, {{ $booking->name }}! <br>
		<br>
		We are glad to inform you that your booking has been confirmed! <br>
		<br>
		Chosen game format: {{ $booking->game->name }} <br>
		<br>
		Number of Participants: {{ $booking->participants }} <br>
		<br>
		Planned Date and Time: {{ $booking->booking_date }} <br>
		<br>
		<br>
		<br>
		See you soon, <br>
		Team Laser Dog

	@endif


</div>