<div>
	@if($booking->locale == 'en')

		Hi, {{ $booking->name }}! <br>
		<br>
		We are sorry to inform you that your booking has been declined. <br>
		<br>
		Nevertheless, we hope to see you soon! <br>
		<br>
		Team Laser Dog

	@elseif($booking->locale == 'lv')

		Sveiki, {{ $booking->name }}! <br>
		<br>
		Vēlamies informāt, ka, diemžēl, Jūsu pieteikums tika noraidīts. <br>
		<br>
		Neskatoties uz to, mēs ceram, ka nākotnē mums izdosies satikties! <br>
		<br>
		Laser Dog komanda

	@elseif($booking->locale == 'ru')

		Hi, {{ $booking->name }}! <br>
		<br>
		We are sorry to inform you that your booking has been declined. <br>
		<br>
		Nevertheless, we hope to see you soon! <br>
		<br>
		Team Laser Dog

	@endif


</div>