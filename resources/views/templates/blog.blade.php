@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row pt-5 pb-0">

			@foreach(\App\Models\Article::all() as $key => $item)
				<div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
					<div class="card article-item">
						<div class="card-header">
							
							@if($item->getFirstMedia('main'))
                                <img src="{{ $item->getFirstMedia('main')->getFullUrl() }}" class="card-img-top">
                            @else
								<img src="{{ asset('img/article_demo_img.png') }}" class="card-img-top">
							@endif
							
							<div class="card-info">
								<i class="fa fa-calendar" aria-hidden="true" style="font-size: 18px; vertical-align: middle; margin-top: -5px; margin-right: 5px;"></i><span class="date">{{ $item->created_at->format('d.m.Y') }}</span>
							</div>
						</div>
						<div class="card-body">
							<h4 class="title text-center px-2"><a href="{{ route('articles.single', $item->slug) }}">{{ $item->title }}</a></h4>
						</div>
					</div>
				</div>
			@endforeach

		</div>

	</div>
</section>

@stop

@push('css')
@endpush

@push('js')
@endpush