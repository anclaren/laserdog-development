@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">

		@foreach(\App\Models\Testimonial::all() as $key => $testimonial)
			<div class="row pt-5">
				@if($key%2)
					<div class="col-12 col-xs-12 col-md-9 col-lg-9 h-100">
						<br>
						<p class="text-justify px-2">{{ $testimonial->content }}</p>
						<div class="px-2 py-1">
							<span class="mt-2 d-inline-block w-100"><b><i>{{ $testimonial->customer }}</i></b></span>
							<span class="d-inline-block w-100"><small><i>- {{ $testimonial->title }}</i></small></span>
						</div>
					</div>
					<div class="col-12 col-xs-12 col-md-3 col-lg-3">
						@if($testimonial->getFirstMedia('photo'))
                            <img src="{{ $testimonial->getFirstMedia('photo')->getFullUrl() }}" class="img-fluid d-none d-md-block d-lg-block">
                        @else
							<img src="{{ asset('img/article_demo_img.png') }}" class="img-fluid d-none d-md-block d-lg-block">
						@endif
					</div>
				@else
					<div class="col-12 col-xs-12 col-md-3 col-lg-3">
						@if($testimonial->getFirstMedia('photo'))
                            <img src="{{ $testimonial->getFirstMedia('photo')->getFullUrl() }}" class="img-fluid d-none d-md-block d-lg-block">
                        @else
							<img src="{{ asset('img/article_demo_img.png') }}" class="img-fluid d-none d-md-block d-lg-block">
						@endif
					</div>
					<div class="col-12 col-xs-12 col-md-9 col-lg-9 h-100">
						<br>
						<p class="text-justify px-2">{{ $testimonial->content }}</p>
						<div class="px-2 py-1">
							<span class="mt-2 d-inline-block w-100"><b><i>{{ $testimonial->customer }}</i></b></span>
							<span class="d-inline-block w-100"><small><i>- {{ $testimonial->title }}</i></small></span>
						</div>
					</div>
				@endif
			</div>
		@endforeach

	</div>
</section>

@stop

@push('css')
@endpush

@push('js')
@endpush