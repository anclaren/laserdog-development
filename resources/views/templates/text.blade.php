@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row pt-5 pb-0">
			<div class="col-12 pb-4 py-5">
				<p class="text-justify">{!! $page->data->text !!}</p>
			</div>
		</div>
	</div>
</section>

@stop

@push('css')
@endpush

@push('js')
@endpush