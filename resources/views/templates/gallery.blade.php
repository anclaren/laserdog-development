@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
@if(isset($page->data->content))
	@foreach($page->data->content as $widget)
		<section class="pb-5">
			<div class="container">

				@if(isset($widget->attributes->items))
					<div class="row">
						<div class="col-12 text-center">
							<ul class="gallery-filter">
								@foreach($widget->attributes->items as $key => $item)
									<li class="nav-item">
										<a href="#" class="nav-link" data-filter=".{{ str_slug($item->attributes->name) }}">{{ $item->attributes->name }}</a>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				@endif

				@if(isset($widget->attributes->items))
					<div class="row pt-2 pb-0 gallery-item-holder">
						@foreach($widget->attributes->items as $key => $item)
							@if(isset($item->attributes->images) && is_array(json_decode($item->attributes->images, true)))
								@foreach(json_decode($item->attributes->images) as $img)
									@php
										$newGenImage = str_replace(['.jpg', '.png', '.jpeg'], '.webp', $img->url);
									@endphp

									<div class="col-lg-4 item {{ str_slug($item->attributes->name) }}" style="margin-bottom: 30px;">
										<a href="{{ $newGenImage }}" class="fancybox" data-fancybox="gallery">
											<img src="{{ $newGenImage }}" class="img-fluid">
										</a>
									</div>
								@endforeach
							@endif
						@endforeach
					</div>
				@endif

			</div>
		</section>
	@endforeach
@endif

@stop

@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endpush

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$(".fancybox").fancybox({
	        openEffect: "none",
	        closeEffect: "none"
	    });

	    /* activate jquery isotope */
	    var $container = $('.gallery-item-holder');

	    $container.isotope({
	        itemSelector: '.item',
	        isFitWidth: true
	    });

	    // trigger isotope again after images have loaded
		$container.imagesLoaded( function(){
			$container.isotope( 'reloadItems' ).isotope();;
		});

	    /*
	    $(window).smartresize(function() {
	        $container.isotope({
	            columnWidth: '.col-sm-3'
	        });
	    });
	    */

	    $container.isotope({
	        filter: '*'
	    });

	    // filter items on button click
	    $('.gallery-filter').on('click', 'a', function(e) {
	    	e.preventDefault();
	        var filterValue = $(this).attr('data-filter');
	        $container.isotope({
	            filter: filterValue
	        });
	    });
	});
</script>
@endpush