@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row pt-5 pb-0">

			@foreach(\App\Models\Game::whereLocale(app()->getLocale())->get() as $key => $game)

				@php
					unset($parts);
					unset($firstpart);
					unset($secondpart);
					unset($thirdpart);
					if(str_word_count_utf8($game->name) >= 2) {
						$parts = explode("\n", wordwrap($game->name, 10, "\n"));
						
						if(isset($parts[0])) { unset($firstpart); $firstpart = $parts[0]; }
						if(isset($parts[1])) { unset($secondpart); $secondpart = $parts[1]; } else { unset($secondpart); }
						if(isset($parts[2])) { unset($thirdpart); $thirdpart = $parts[2]; } else { unset($thirdpart); }
					}
				@endphp

				<div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 pb-4">
					<div class="card category-item">
						<a href="{{ route('games.single', $game->slug) }}">
							
							<picture class="card-img-top">
								@if($game->getFirstMedia('main'))
	                                <source srcset="{{ str_replace('.jpg', '.webp', $game->getFirstMedia('main')->getFullUrl('preview_thumb')) }}" type="image/webp">
									<source srcset="{{ str_replace('.jpg', '.jpeg', $game->getFirstMedia('main')->getFullUrl('preview_thumb')) }}" type="image/jpeg"> 
									<img src="{{ $game->getFirstMedia('main')->getFullUrl('preview_thumb') }}" class="card-img-top" alt="{{ $game->name }}">
	                            @else
									<img src="{{ asset('img/category_item.jpg') }}" class="card-img-top" alt="{{ $game->name }}">
								@endif
							</picture>
							
							<div class="card-body">
								<div class="text-content">
									<h2>
										{{ (isset($firstpart) ? $firstpart : $game->name ) }}<br> @if(isset($secondpart)) <span class="text-color">{{ $secondpart }}</span> @endif
										@if(isset($thirdpart)) <br> {{ $thirdpart }} @endif
									</h2>
								</div>
							</div>
						</a>
					</div>
				</div>
			@endforeach

		</div>
	</div>
</section>

@stop

@push('css')
@endpush

@push('js')
@endpush