@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<header>
	<div id="carouselExampleIndicators" class="homepage-carousel carousel slide" data-ride="carousel">

		<div class="carousel-indicators">
        <a href="#whatIsLasertag" class="scrollTo animated bounce" aria-label="ScrollToCategories">
        	<img src="{{ asset('img/slider_anchor.png') }}" class="img-fluid" style="max-width: 50px;" alt="">
        </a>
    </div>

		<div class="carousel-inner">

            @foreach(\App\Models\SliderSlide::whereLocale(app()->getLocale())->get() as $key => $slide)
            	@if(!$slide->getFirstMedia('main')) @continue @endif
        			<div class="carousel-item {{ (($key == 0) ? 'active' : '') }}" style="background-image: 
                  url('{{ str_replace('.jpg', '.webp', $slide->getFirstMedia('main')->getFullUrl('preview')) }}'),
                  url('{{ str_replace('.jpg', '.jpeg', $slide->getFirstMedia('main')->getFullUrl('preview')) }}'),
                  url('{{ $slide->getFirstMedia('main')->getFullUrl('preview') }}');
                ">
        				<div class="carousel-caption d-md-block">
        					<h1 class="display-1">
        						{{ $slide->title }} <br>
        						<b>{{ $slide->sub_title }}</b>
        					</h1>
        				</div>
        			</div>
            @endforeach

		</div>
	</div>
</header>

<!-- Widgets start -->
@if(isset($page->data->content))
    @foreach($page->data->content as $key => $widget)
        @include('layouts.widgets.' . $widget->layout, ['widgetData' => $widget->attributes])
    @endforeach
@endif
<!-- Widgets end -->

@stop

@push('css')
@endpush

@push('js')
<script>
$('.category-carousel').owlCarousel({
    items: 4,
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    margin: 20,
    nav: false,
    dots: false,
    navText: ['<span class="fas fa-chevron-left"></span>','<span class="fas fa-chevron-right"></span>'],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false,
            loop: true,
        },
        600:{
            items:2,
            nav:false,
            loop: true,
        },
        1000:{
            items:4,
            nav:false,
            loop:true,
            dots: true,
        }
    }
});

google.maps.event.addDomListener(window, 'load', init);

function init() {
    var image = "{{ asset('img/map-pin.png') }}";
    var mapOptions = {
        zoom: 9,
        zoomControl: false,
        fullscreenControl: false,
        disableDoubleClickZoom: false,
        mapTypeControl: false,
        scaleControl: false,
        scrollwheel: false,
        panControl: true,
        streetViewControl: false,
        draggable: true,
        center: new google.maps.LatLng(56.85148, 24.149425),
        styles: [
            {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
        ]
    };

    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(56.922228, 24.095920),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });
    var marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(56.956551, 24.073310),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });
    var marker3 = new google.maps.Marker({
        position: new google.maps.LatLng(56.654384, 23.757830),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
}
</script>
@endpush