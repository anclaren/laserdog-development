@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($page->seo_title) && $page->seo_title) ? $page->seo_title : $page->name))
@section('meta_keywords',  ((isset($page->seo_keywords) && $page->seo_keywords) ? $page->seo_keywords : ''))
@section('meta_description',  ((isset($page->seo_description) && $page->seo_description) ? $page->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $page->name }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row pt-5 pb-0">
			
			<div class="col-12 col-sm-12 col-md-6 col-xl-6 col-lg-6 pb-4">
				<div class="contact-info-block text-center pt-4">

					@if(isset($page->data->blocks))
						@foreach($page->data->blocks as $key => $block)
							<h2 class="contact-detail"><i class="fa {{ $block->icon }}"></i> {{ $block->text }}</h2>
						@endforeach
					@endif

					<br>
					<br>

					@if(isset($page->data->requisites))
						@foreach($page->data->requisites as $key => $requisite)
							<h2 class="requisite-detail">{{ $key }}: <span>{{ $requisite }}</span></h2>
						@endforeach
					@endif

				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-xl-6 col-lg-6 pb-4">

        @if(Session::has('statusFail'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('statusFail') }}
          </div>
        @endif
				
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

				<form class="contact-form contacts-page-form" method="POST" action="{{ route('form.contacts') }}">
					@csrf
					<div class="form-group">
						<label>{{ __('form.name') }}</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ __('form.email') }}</label>
						<input type="email" name="email" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ __('form.message') }}</label>
						<textarea class="form-control" name="message" rows="5"></textarea>
					</div>

					<button type="submit" class="btn btn-block">{{ __('form.send') }} <i class="fa fa-arrow-right"></i></button>
				</form>
			</div>

		</div>

	</div>
</section>

<section class="map-container">
	<div id="map">{{ __('contacts.map') }}</div>
</section>

@stop

@push('css')
@endpush

@push('js')
<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', init);

function init() {
    var image = "{{ asset('img/map-pin.png') }}";
    var mapOptions = {
        zoom: 9,
        zoomControl: true,
        disableDoubleClickZoom: false,
        mapTypeControl: false,
        scaleControl: false,
        scrollwheel: false,
        panControl: true,
        streetViewControl: false,
        draggable: true,
        center: new google.maps.LatLng(56.85148, 24.149425),
        styles: [
            {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
        ]
    };

    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(56.922228, 24.095920),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });
    var marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(56.956551, 24.073310),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });
    var marker3 = new google.maps.Marker({
        position: new google.maps.LatLng(56.654384, 23.757830),
        map: map,
        icon: image,
        title: "{{ __('Our Location') }}",
        animation: google.maps.Animation.DROP,
    });

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
}

$(function(){
  $('body .contacts-page-form').on('submit', function(e) {
    e.preventDefault();
    
    $('body .contacts-page-form').find('button').attr('disabled', true);
    $('body .contacts-page-form')[0].submit();
  });
});
</script>
@endpush
