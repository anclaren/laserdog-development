@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($game->seo_title) && $game->seo_title) ? $game->seo_title : $game->name))
@section('meta_keywords',  ((isset($game->seo_keywords) && $game->seo_keywords) ? $game->seo_keywords : ''))
@section('meta_description',  ((isset($game->seo_description) && $game->seo_description) ? $game->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $game->name }}</h1>
		</div>
	</div>
</section>

<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row single-game-view pt-5 pb-0">

			@if(isset($game->data))
				@foreach($game->data as $key => $info)
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<h2>{{ (isset($info['title']) ? $info['title'] : '') }}</h2>
						<p>{!! (isset($info['text']) ? nl2br($info['text']) : '') !!}</p>
					</div>
				@endforeach
			@endif

			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="gallery py-4">
					@if(count($game->getMedia('game_gallery')) > 0)
						<h2 class="pb-2">{{ __('Gallery') }}</h2>
						
						@php
							$allImagesInGallery = $game->getMedia('game_gallery');
							$totalGalleryImages = $game->getMedia('game_gallery')->count();
							$imagesLeftUnrevealed = $totalGalleryImages - 8;
						@endphp

						@if($imagesLeftUnrevealed > 0)
							<style type="text/css">
								.single-game-view {}
								.single-game-view .gallery {}
								.single-game-view .gallery a.thumbnail.fancybox.show-more::after {
									content: '+{{ $imagesLeftUnrevealed }}';
									position: absolute;
									right: 0;
									left: 0;
									top: 0;
									bottom: 0;
									background: #000000ad;
									z-index: 1;
									opacity: 1;
									text-align: center;
									line-height: 100px;
									font-size: 30px;
								}
							</style>
						@endif

						<div class="row px-2"> 
							@foreach($allImagesInGallery as $key => $gallery_item)
								<div class="{{ ($key == 0 || $key == 1) ? 'col-md-6 col-6' : 'col-md-4 col-6' }} px-0 py-0 mb-0 mx-0" style="{{ ($key > 7) ? 'display: none;' : '' }}">
									<a class="thumbnail fancybox {{ ($key == 7 && count($allImagesInGallery) > 7) ? 'show-more' : '' }}" data-fancybox="gallery" href="{{ $gallery_item->getFullUrl() }}">

										<picture class="img-fluid">
											@if($key == 0 || $key == 1)
												<source srcset="{{ str_replace('.jpg', '.webp', $gallery_item->getFullUrl('gallery_large_thumb')) }}" type="image/webp">
												<source srcset="{{ str_replace('.jpg', '.jpeg', $gallery_item->getFullUrl('gallery_large_thumb')) }}" type="image/jpeg"> 
												<img src="{{ $gallery_item->getFullUrl('gallery_large_thumb') }}" alt="" class="img-fluid">
											@else
												<source srcset="{{ str_replace('.jpg', '.webp', $gallery_item->getFullUrl('gallery_small_thumb')) }}" type="image/webp">
												<source srcset="{{ str_replace('.jpg', '.jpeg', $gallery_item->getFullUrl('gallery_small_thumb')) }}" type="image/jpeg"> 
												<img src="{{ $gallery_item->getFullUrl('gallery_small_thumb') }}" alt="" class="img-fluid">
											@endif
										</picture>
									</a>
								</div>
							@endforeach
						</div>
					@endif

					<div class="row mt-3">
						<div class="col-md-12">
							<a href="{{ url('games') }}"><i class="fa fa-arrow-left"></i> {{ __('btn.all_games') }}</a>
						</div>
					</div>

				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

				@if(Session::has('statusFail'))
					<div class="alert alert-danger" role="alert">
						{{ Session::get('statusFail') }}
					</div>
				@endif

				@if(Session::has('status'))
					<div class="alert alert-success" role="alert">
						{{ Session::get('status') }}
					</div>
				@endif
				
				<form class="contact-form single-game-form" method="POST" action="{{ route('form.inquiry') }}">
					
					@csrf
					<input type="hidden" name="game_id" value="{{ $game->id }}">
					<input type="hidden" name="locale" value="{{ app()->getLocale() }}">

					<div class="form-group">
						<label class="col-form-label text-left">@lang('form.name')</label>
						<input type="text" class="form-control" name="name">
					</div>
					<div class="form-group">
						<label class="col-form-label text-left">@lang('form.phone')</label>
						<input type="text" class="form-control" name="phone">
					</div>
					<div class="form-group">
						<label class="col-form-label text-left">@lang('form.email')</label>
						<input type="email" class="form-control" name="email">
					</div>
					<div class="form-group">
						<label class="col-form-label text-left">@lang('form.participants')</label>
						<select class="form-control" name="participants">
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value=">12">> 12</option>
						</select>
					</div>
					<div class="form-group">
						<label class="col-form-label">@lang('form.datetime')</label>
						<div class="input-group date laser-date-picker" id="singleBookingDateTimePicker" data-target-input="nearest">
		                    <input type="text" class="form-control datetimepicker-input" name="booking_date" data-target="#singleBookingDateTimePicker"/>
		                    <div class="input-group-append" data-target="#singleBookingDateTimePicker" data-toggle="datetimepicker">
		                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                    </div>
		                </div>
					</div>

					<button class="btn btn-block" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="@lang('form.apply_text')">@lang('form.apply_to_game') <i class="fa fa-arrow-right"></i></button>
				</form>
			</div>
		</div>

	</div>
</section>

@stop

@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endpush

@push('js')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

$(function () {
    $('#singleBookingDateTimePicker').datetimepicker({
    	locale: '{{ app()->getLocale() }}',
    	format: 'YYYY-MM-DD HH:mm:00',
    	stepping: 30,
    	sideBySide: true,
    	allowInputToggle: true,
    	keepOpen: false,
    });

    $('body .single-game-form').on('submit', function(e) {
    	e.preventDefault();
    	
    	$('body .single-game-form').find('button').attr('disabled', true);
		$('body .single-game-form')[0].submit();
    });

});

$(document).ready(function(){
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });

    $(document).on('click', 'a.show-more', function(e){

    	var $this = $(this),
    		insertAfterElement = $this.parent();

    	//$this.toggleClass('show-more').toggleClass('fancybox');;

    	//e.preventDefault();
    	$.ajax({
    		url: '{{ route('game.loadMoreImages') }}',
    		type: 'POST',
    		dataType: 'json',
    		data: { "_token": "{{ csrf_token() }}", game_id: '{{ $game->id }}'},
    		success: function(result) {
    			$.each(result, function(index, val) {

    				var newHtml = '<div class="col-md-4 px-0 py-0 mb-0 mx-0" style="display:none;">'+
									'<a class="thumbnail fancybox" rel="ligthbox" href="' + val.full + '">'+
										'<img src="' + val.thumb + '" class="img-fluid">'+
									'</a>'+
								'</div>';

    				$(newHtml).insertAfter(insertAfterElement);
    			});
    			
    			reloadFancybox(); // Reload Fancybox

    		}
    	});
    });

    function reloadFancybox(){
    	$(".fancybox").fancybox({
	        openEffect: "none",
	        closeEffect: "none"
	    });
    }

});
</script>
@endpush