@if(isset($widgetData->attributes->dynamic_blocks))
<div class="row py-5">
	@foreach($widgetData->attributes->dynamic_blocks as $key => $block)
		@if(isset($block->attributes))
			<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 pb-4">
				<div class="pricing-info-box">
					<h2>{{ (isset($block->attributes->title) ? $block->attributes->title : '') }}</h2>

					@if(isset($block->attributes->info_blocks))
						@foreach($block->attributes->info_blocks as $k => $value)
							<p class="paragraph">{{ $value->name }} <b class="float-right">{{ $value->text }}</b></p>
						@endforeach
					@endif

				</div>
			</div>
		@endif
	@endforeach
</div>
@endif