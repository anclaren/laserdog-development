<div class="row py-3">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		@if(isset($widgetData->attributes->image))
			<div style="
				background: url({{ Storage::disk('public')->url($widgetData->attributes->image) }}); 
				height: 350px; 
				width: 100%; 
				background-size: cover; 
				background-position: center;">
		@endif
	</div>
</div>