@extends('layouts.app')
@section('meta_title', ' - ' . ((isset($article->seo_title) && $article->seo_title) ? $article->seo_title : $article->title))
@section('meta_keywords',  ((isset($article->seo_keywords) && $article->seo_keywords) ? $article->seo_keywords : ''))
@section('meta_description',  ((isset($article->seo_description) && $article->seo_description) ? $article->seo_description : ''))
@section('content')

<section class="page-header container-fluid" style="background-image: url('{{ asset('img/slider/main.jpg') }}');">
	<div class="row h-100">
		<div class="col-sm-12 my-auto text-center">
			<h1>{{ $article->title }}</h1>
		</div>
	</div>
</section>


<!-- Page Content -->
<section class="pb-5">
	<div class="container">
		<div class="row pt-5 pb-0">
			
			<div class="col-12 pb-4 text-center">
				{!! $article->content !!}
			</div>
			
		</div>
	</div>
</section>

@stop

@push('css')
@endpush

@push('js')
@endpush