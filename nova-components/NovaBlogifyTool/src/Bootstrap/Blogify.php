<?php

namespace Anclaren\NovaBlogifyTool\Bootstrap;

use Laravel\Nova\Nova;
use Illuminate\Support\Facades\Schema;
use Anclaren\NovaBlogifyTool\Resources\Tag;
use Anclaren\NovaBlogifyTool\Resources\Post;
use Anclaren\NovaBlogifyTool\Resources\Image;
use Anclaren\NovaBlogifyTool\Resources\Comment;
use Anclaren\NovaBlogifyTool\Resources\Category;

class Blogify
{
    public static function isInstalled()
    {
        return
            Schema::hasTable('posts') &&
            Schema::hasTable('categories') &&
            Schema::hasTable('comments') &&
            Schema::hasTable('tags') &&
            Schema::hasTable('post_tag') &&
            Schema::hasTable('images');
    }

    public static function injectToolResources()
    {
        if (! self::isInstalled()) {
            return;
        }

        Nova::resources([
            Category::class,
            Post::class,
            Comment::class,
            Tag::class,
            Image::class,
        ]);
    }
}
