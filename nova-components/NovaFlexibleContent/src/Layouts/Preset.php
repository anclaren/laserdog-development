<?php

namespace Anclaren\NovaFlexibleContent\Layouts;

use Anclaren\NovaFlexibleContent\Flexible;

abstract class Preset
{
    /**
     * Execute the preset configuration
     *
     * @return void
     */
    abstract public function handle(Flexible $field);

}
