<?php

namespace Anclaren\NovaFlexibleContent;

use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\ServiceProvider;
use Anclaren\NovaFlexibleContent\Commands\CreateLayout;
use Anclaren\NovaFlexibleContent\Commands\CreatePreset;
use Anclaren\NovaFlexibleContent\Commands\CreateResolver;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('nova-flexible-content', __DIR__.'/../dist/js/field.js');
            Nova::style('nova-flexible-content', __DIR__.'/../dist/css/field.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!$this->app->runningInConsole()) return;

        $this->commands([
            CreateLayout::class,
            CreatePreset::class,
            CreateResolver::class,
        ]);
    }
}
