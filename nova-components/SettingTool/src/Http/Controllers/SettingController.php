<?php

namespace Anclaren\SettingTool\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

// Models
use Anclaren\SettingTool\Models\Setting;

class SettingController
{

    /**
     * Response setting.
     * @var string
     */
    protected $setting;

    /**
     * Response status.
     * @var string
     */
    protected $statusCode = 200;

    public function list(): JsonResponse
    {
        $this->setting = [
            'app' => Setting::pluck('value','key')->toArray()
        ];

        return response()->json([
            'setting' => $this->setting
        ], $this->statusCode);
    }

    /**
     * Update setting.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        try {

            foreach ($request->get('app') as $key => $value) {
                Setting::updateOrCreate(['key' => $key],['value' => $value]);
            }

            $this->setting = Setting::all()->toArray();
            $this->message = 'The setting was updated. The page will refresh in few seconds...';
            
        } catch (\Exception $e) {
            $this->message = 'Uh-oh, unable to update setting.';
            $this->statusCode = 400;
        }

        return response()->json([
            'message' => $this->message,
            'setting' => $this->setting,
        ], $this->statusCode);
    }

}