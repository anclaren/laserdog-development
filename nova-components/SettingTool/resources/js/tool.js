Nova.booting((Vue, router) => {
    router.addRoutes([
        {
            name: 'setting-tool',
            path: '/setting-tool',
            component: require('./components/Tool'),
        },
    ])
})
