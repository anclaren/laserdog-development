<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/default-setting', 'Anclaren\SettingTool\Http\Controllers\SettingController@list');
Route::put('/update-setting', 'Anclaren\SettingTool\Http\Controllers\SettingController@update');