<?php

namespace Anclaren\NovaPageManager\Http;

use Illuminate\Routing\Controller;
use Laravel\Nova\Http\Requests\NovaRequest;
use Anclaren\NovaPageManager\Models\Page;
use Anclaren\NovaPageManager\Models\Region;

class PageAndRegionController extends Controller
{
    public function getPagesAndRegions(NovaRequest $request)
    {
        $pages = Page::all();
        $regions = Region::all();

        return [
            'pages' => $pages,
            'regions' => $regions
        ];
    }
}
