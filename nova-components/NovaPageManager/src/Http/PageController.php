<?php

namespace Anclaren\NovaPageManager\Http;

// Laravel & Nova
use Illuminate\Http\Request;

// Models
use Anclaren\NovaPageManager\Models\Page;

class PageController extends \App\Http\Controllers\Controller
{
    public function page($slug = null)
    {
        $currentLocale = app()->getLocale();

        // Exploding language from slug
        if($slug !== null) {
            $slug = explode('/', $slug);

            if(isset($slug[0]) && $slug[0] == $currentLocale) {
                if(!isset($slug[1])) {
                    $slug = null;
                } else {
                    $slug = $slug[1];
                }
            } else {
                $slug = $slug[0];
            }
        }

        // Define slug
        $slug = $slug ? $slug : '/';

        // Check if current page exists
        if($slug == '/') {
            $page = Page::whereLocale($currentLocale)->whereTemplate('homepage')->first();
        } else {

            // Check if current slug is correct for selected locale
            $page = Page::whereLocale($currentLocale)->whereSlug($slug)->first();
            // If Page does not exists, redirect to current page but correct locale
            if(!$page) {

                /*
                * TODO
                |
                | Create normal switch for translatable pages
                |
                */

                return redirect($currentLocale);
            }
        }

        if (!$page) {
            return abort(404);
        }
        
        return view(config('nova-page-manager.templates_location').'.'.$page->template, compact('page'));
    }

    protected function searchPageFromOtherLocale(Page $page) {

    }
}
