<?php

namespace Anclaren\NovaPageManager\Models;

use Anclaren\NovaPageManager\NovaPageManager;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Page extends TemplateModel
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(NovaPageManager::getPagesTableName());
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if(!$model->slug) {
                $model->slug = str_slug($model->name);
                $latestSlug = static::whereRaw("slug = '$model->slug' or slug LIKE '$model->slug-%'")->latest('id')->value('slug');
                
                if ($latestSlug) {
                    $pieces = explode('-', $latestSlug);
                    $number = intval(end($pieces));
                    $model->slug .= '-' . ($number + 1);
                }
            }
        });

        static::saving(function($model) {

            if (isset($page->draft) && NovaPageManager::draftEnabled()) {
                unset($page['draft']);
                return Page::createDraft($page);
            }
            
            $imageArr = [];

            if(isset($model->data->content)) {

                $data = static::object_to_array($model->data->content);

                if($model->template == 'gallery') {
                    return static::optimizeGalleryImages($data);
                }
                
                foreach ($data as $key => $value) {
                    $img = array_column($value, 'image');

                    if($img) {
                        $imageArr[] = $img[0];
                    }

                    if(isset($value['attributes']['images'])) {
                        $multipleImages = array_column(json_decode($value['attributes']['images'], true), 'url');

                        foreach ($multipleImages as $k => $v) {
                            $imageArr[] = str_replace(url('storage/images') . '/', '', $v);
                        }
                    }

                    if(isset($value['attributes']['main_image'])){
                        $imageArr[] = $value['attributes']['main_image'];
                    }
                    if(isset($value['attributes']['cover_image'])){
                        $imageArr[] = $value['attributes']['cover_image'];
                    }
                }

                if(count($imageArr) > 0) {
                    foreach ($imageArr as $key => $image) {
                        list($name, $extension) = explode('.', $image);
                        $file = Storage::disk('public')->path($image);

                        if(file_exists($file)) {
                            \Intervention\Image\Facades\Image::make($file)->encode('webp')->save(storage_path('app/public/' . $name.'.webp'));
                            \Intervention\Image\Facades\Image::make($file)->encode('jpg')->save(storage_path('app/public/' . $name.'.jpg'));
                        } else {
                            $file = Storage::disk('public')->path('images/' . $image);

                            if(file_exists($file)) {
                                \Intervention\Image\Facades\Image::make($file)->encode('webp')->save(storage_path('app/public/' . $name.'.webp'));
                                \Intervention\Image\Facades\Image::make($file)->encode('jpg')->save(storage_path('app/public/' . $name.'.jpg'));
                            }
                        }
                    }
                }

            }

            return true;
        });

        static::deleting(function ($template) {
            // Is a parent template
            if ($template->parent_id === null) {
                // Find child templates
                $childTemplates = Page::where('parent_id', '=', $template->id)->get();
                if (count($childTemplates) === 0) return;

                // Set their parent to null
                $childTemplates->each(function ($template) {
                    $template->update(['parent_id' => null]);
                });
            }
        });
    }

    static protected function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = static::object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;       
    }

    private static function createDraft($pageData) {
        if (isset($pageData->id)) {
            $newPage = $pageData->replicate();
            $newPage->published = false;
            $newPage->draft_parent_id = $pageData->id;
            $newPage->preview_token = Str::random(20);
            $newPage->save();
            return false;
        } 
        
        $pageData->published = false;
        $pageData->preview_token = Str::random(20);
        return true;
    }

    private static function optimizeGalleryImages($data = [])
    {
        $imageArray = [];

        // Iterating through blocks
        foreach ($data as $block_id => $block) {
            // Check if gallery items exists
            if(isset($block['attributes']['items'])) {
                // Iterating through items and collecting data
                foreach ($block['attributes']['items'] as $key => $value) {

                    $images = array_column(json_decode($value['attributes']['images'], true), 'url');

                    foreach ($images as $k => $v) {
                        $replacement = str_replace(['https://laserdog.anc-dev.info/storage/images/', 'https://laserdog.lv/storage/images/'], '', $v);
                        $imageArray[$key][] = $replacement;
                    }

                }
            }
        }

        // Merging data into single array
        $result = array_merge(...$imageArray);

        // Iterating through collected data and optimize images
        foreach ($result as $key => $image) {
            $file = Storage::disk('public')->path($image);

            // File info
            $infoPath = pathinfo($file);
            $_filename = $infoPath['filename'];
            $_extension = $infoPath['extension'];

            // Check if image already formated to new gen format
            if(file_exists(storage_path('app/public/' . $_filename.'.webp')) || file_exists(storage_path('app/public/images/' . $_filename.'.webp'))) {
                //continue;
            }

            if(file_exists($file)) {
                \Intervention\Image\Facades\Image::make($file)->encode('webp', 80)->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(storage_path('app/public/' . $_filename.'.webp'));
                \Intervention\Image\Facades\Image::make($file)->encode('jpeg', 80)->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(storage_path('app/public/' . $_filename.'.jpeg'));
            } else {
                $file = Storage::disk('public')->path('images/' . $image);
                // File info
                $infoPath = pathinfo($file);
                $_filename = $infoPath['filename'];
                $_extension = $infoPath['extension'];

                if(file_exists($file)) {
                    \Intervention\Image\Facades\Image::make($file)->encode('webp', 80)->resize(1024, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(storage_path('app/public/images/' . $_filename.'.webp'));
                    \Intervention\Image\Facades\Image::make($file)->encode('jpeg', 80)->resize(1024, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(storage_path('app/public/images/' . $_filename.'.jpeg'));
                }
            }
        }

        return true;
    }
    
    public function parent()
    {
        return $this->belongsTo(Page::class);
    }

    public function draftParent() 
    {
        return $this->belongsTo(Page::class);
    }

    public function childDraft() {
        return $this->hasOne(Page::class, 'draft_parent_id', 'id');
    }
}
