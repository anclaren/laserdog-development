<?php

namespace Anclaren\MenuBuilder\Http\Models;

use Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Route;
use Anclaren\MenuBuilder\Http\Models\Menu;

class MenuItems extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['menu_id', 'name', 'url', 'route', 'page', 'parameters', 'target', 'parent_id', 'order', 'enabled', 'classes'];

    /**
     * @var mixed
     */
    protected $with = ['children'];

    /**
     * @var array
     */
    protected $casts = [
        'enabled' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $appends = ['link', 'type', 'enabledClass'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id')
            ->orderBy('order', 'asc')
            ->with('children');
    }

    /**
     * @param $parentId
     *
     * @return Collection
     */
    public function itemsChildren($parentId)
    {
        return $this->whereParentId($parentId);
    }

    /**
     * Get Type attribute
     *
     * @return  string
     */
    public function getTypeAttribute()
    {
        if ($this->route != null) {
            return 'route';
        }

        if($this->page != null) {
            return 'page';
        }

        return 'link';
    }

    /**
     * Get Enabled class attribute
     *
     * @return  string
     */
    public function getEnabledClassAttribute()
    {
        return ($this->enabled) ? 'enabled' : 'disabled';
    }

    /**
     * Get the link attribute
     *
     * @param   boolean  $absolute
     *
     * @return  string
     */
    public function getLinkAttribute($absolute = false)
    {
        if (is_null($this->parameters)) {
            $this->parameters = [];
        }

        if (is_string($this->parameters)) {
            $this->parameters = json_decode($this->parameters, true);
        } elseif (is_array($this->parameters)) {
            $this->parameters = $this->parameters;
        } elseif (is_object($this->parameters)) {
            $this->parameters = json_decode(json_encode($this->parameters), true);
        }

        if (!is_null($this->route)) {
            if (!Route::has($this->route)) {
                return '#';
            }

            return route($this->route, $this->parameters, $absolute);
        }

        if (starts_with($this->url, 'http')) {
            $absolute = true;
        }

        if ($absolute) {
            return url($this->url);
        }

        if(class_exists('Anclaren\NovaPageManager\Models\Page')) {
            if(!is_null($this->page)) {

                $currPage = \Anclaren\NovaPageManager\Models\Page::find($this->page);

                if(app()->getLocale() == $currPage->locale) {

                    if($currPage->template == 'homepage') {
                        return url('/');
                    } else {
                        return url($currPage->slug);
                    }

                } else {

                    $currPageLocale = \Anclaren\NovaPageManager\Models\Page::whereLocale(app()->getLocale())->whereLocaleParentId($currPage->id)->first();
                    if(isset($currPageLocale)) {
                        if($currPageLocale->template == 'homepage') {
                            return url('/');
                        } else {
                            return url($currPageLocale->slug);
                        }
                    } else {
                        return url('/');
                    }
                }

            }
        }

        return $this->url;
    }

    /**
     * Return the html link
     *
     * @return  string
     */
    public function html($linkClass = null)
    {
        $isDropdownItem = ($this->parent_id != null) ? ' anc-dropdown-item' : '';

        $currentLinkClass = 'nav-link';

        if($linkClass) {
            $currentLinkClass = 'nav__link';
        }

        return '<a class=" '.$currentLinkClass.' '. $this->classes . $isDropdownItem . '" href="'.$this->link.'" target="'.$this->target.'">'.$this->name.'</a>';
    }

}