<?php

namespace Anclaren\MenuBuilder\Nova;

use Laravel\Nova\Resource;
use Illuminate\Http\Request;
use Anclaren\MenuBuilder\Nova\Filters\MenuLocaleFilter;
use Anclaren\MenuBuilder\Nova\Filters\MenuChildrenFilter;

abstract class TemplateResource extends Resource
{
    public function filters(Request $request)
    {
        return [
            new MenuLocaleFilter,
            new MenuChildrenFilter,
        ];
    }
}
