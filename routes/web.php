<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test-reviews', function(){
	require_once '../app/Services/GoogleMyBusiness.php';
	$client = new Google_Client();
	$client->setAuthConfig(base_path('app/Services/LaserDog-Reviews-API-d78198403697.json'));
	$client->addScope("https://www.googleapis.com/auth/plus.business.manage");
	$myBusinessService = new Google_Service_Mybusiness($client);

	$accounts = $myBusinessService->accounts->listAccounts()->getAccounts();

	dd($accounts);


// Retrieves basic insights for a given list of locations.
	$reportLocationInsightsRequest = new Google_Service_MyBusiness_ReportLocationInsightsRequest(); 
	$basicRequest = new Google_Service_MyBusiness_BasicMetricsRequest(); 
	$metricRequests = new Google_Service_MyBusiness_MetricRequest(); 
	$metricRequests->setMetric("ALL"); 
	$basicRequest->setMetricRequests($metricRequests); 
	$timeRange = new Google_Service_MyBusiness_TimeRange(); 
	$timeRange->setStartTime("2016-10-12T01:01:23.045123456Z"); 
	$timeRange->setEndTime("2017-01-10T23:59:59.045123456Z"); 
	$basicRequest->setTimeRange($timeRange); 
	$reportLocationInsightsRequest->setBasicRequest($basicRequest); 
	$reportLocationInsightsRequest->setLocationNames(array($location->name)); 
	$reportLocationInsightsResponse = $locations->reportInsights($account->name, $reportLocationInsightsRequest);
	$locationMetrics = $reportLocationInsightsResponse->getLocationMetrics(); 
	$locationMetricsArray = array(); 
	$locationMetricsArray['locationMetrics']['locationName'] = array($locationMetrics[0]->getLocationName()); 
	$locationMetricsArray['locationMetrics']['timeZone'] = array($locationMetrics[0]->getTimeZone()); 
	$metricValuesArray = array();
	
	foreach ($locationMetrics[0]->getMetricValues() as $value) { 
		$metricValuesItem = array(); 
		$metricValuesItem['metric'] = $value['metric']; 
		$metricValuesItem['totalValue']['timeDimension']['timeRange']['endTime'] = $value['totalValue']['timeDimension']['timeRange']['endTime']; 
		$metricValuesItem['totalValue']['timeDimension']['timeRange']['startTime'] = $value['totalValue']['timeDimension']['timeRange']['startTime']; 
		$metricValuesItem['totalValue']['metricOption'] = $value['totalValue']['metricOption']; 
		$metricValuesItem['totalValue']['value'] = $value['totalValue']['value']; 
		$metricValuesArray[] = $metricValuesItem;
	} 
	
	$locationMetricsArray['locationMetrics']['metricValues'] = $metricValuesArray; 
	print json_encode($locationMetricsArray, JSON_PRETTY_PRINT) . "\n";

	dd(1);

	$reportInsightsArr = array('locationNames' => 'accounts/xxx/locations/yyy');
	$response = $myBusinessService->accounts_locations->reportInsights('laserdog',$reportInsightsArr);
	dd($response);
});

Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [
		'localeSessionRedirect', 
		'localizationRedirect', 
		'localeViewPath' 
	]
], function()
{
	
	Route::get(__('routes.games') . '/{slug}', 'GameController@show')->name('games.single');
	Route::get(__('routes.articles') . '/{slug}', 'ArticleController@show')->name('articles.single');
		
	Route::post('send-inquiry', 'FormController@sendInquiry')->name('form.inquiry');
	Route::post('send-contacts-request', 'FormController@contactForm')->name('form.contacts');

	Route::post('load-more-images', 'GameController@loadMoreImages')->name('game.loadMoreImages');

	/*
	Route::post('accept-cookies', 'CookieController@accept')->name('cookies.accept');
	Route::post('decline-cookies', 'CookieController@decline')->name('cookies.decline');
	*/
});

if(class_exists('Anclaren\NovaPageManager\NovaPageManager') && class_exists('Mcamara\LaravelLocalization\Facades\LaravelLocalization')) {

	Route::group([
		'middleware' => [
			'localeSessionRedirect', 
			'localizationRedirect', 
			'localeViewPath' 
		]
	], function() {

		Route::get('{slug}/{param?}', '\Anclaren\NovaPageManager\Http\PageController@page')
			->where('slug', '^(?!nova|'.trim(config('nova.path'), '/').'.*$).*');

	});
	
}